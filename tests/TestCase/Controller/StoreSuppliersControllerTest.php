<?php
namespace App\Test\TestCase\Controller;

use App\Controller\StoreSuppliersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\StoreSuppliersController Test Case
 */
class StoreSuppliersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.store_suppliers',
        'app.stores',
        'app.stocktakes',
        'app.products',
        'app.price_checks',
        'app.suppliers',
        'app.same_day_orders',
        'app.user_stores',
        'app.users',
        'app.orders',
        'app.invoices'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
