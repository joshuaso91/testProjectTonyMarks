<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PriceChecksController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\PriceChecksController Test Case
 */
class PriceChecksControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.price_checks',
        'app.suppliers',
        'app.same_day_orders',
        'app.user_stores',
        'app.users',
        'app.stores',
        'app.stocktakes',
        'app.products',
        'app.store_suppliers',
        'app.orders',
        'app.invoices'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test addSupplierProducts method
     *
     * @return void
     */
    public function testAddSupplierProducts()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
