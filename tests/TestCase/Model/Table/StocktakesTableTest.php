<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StocktakesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StocktakesTable Test Case
 */
class StocktakesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StocktakesTable
     */
    public $Stocktakes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.stocktakes',
        'app.stores',
        'app.store_suppliers',
        'app.suppliers',
        'app.price_checks',
        'app.products',
        'app.same_day_orders',
        'app.user_stores',
        'app.users',
        'app.orders',
        'app.invoices'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Stocktakes') ? [] : ['className' => 'App\Model\Table\StocktakesTable'];
        $this->Stocktakes = TableRegistry::get('Stocktakes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Stocktakes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
