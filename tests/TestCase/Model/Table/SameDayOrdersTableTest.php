<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SameDayOrdersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SameDayOrdersTable Test Case
 */
class SameDayOrdersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SameDayOrdersTable
     */
    public $SameDayOrders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.same_day_orders',
        'app.user_stores',
        'app.users',
        'app.stores',
        'app.stocktakes',
        'app.products',
        'app.price_checks',
        'app.suppliers',
        'app.store_suppliers',
        'app.orders',
        'app.invoices'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SameDayOrders') ? [] : ['className' => 'App\Model\Table\SameDayOrdersTable'];
        $this->SameDayOrders = TableRegistry::get('SameDayOrders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SameDayOrders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
