<?php
namespace App\Test\TestCase\Mailer;

use App\Mailer\RegistrationFormMailer;
use Cake\TestSuite\TestCase;

/**
 * App\Mailer\RegistrationFormMailer Test Case
 */
class RegistrationFormMailerTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Mailer\RegistrationFormMailer
     */
    public $RegistrationForm;

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
