<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Stores Model
 *
 * @property \Cake\ORM\Association\HasMany $Stocktakes
 * @property \Cake\ORM\Association\HasMany $StoreSuppliers
 * @property \Cake\ORM\Association\HasMany $UserStores
 *
 * @method \App\Model\Entity\Store get($primaryKey, $options = [])
 * @method \App\Model\Entity\Store newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Store[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Store|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Store patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Store[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Store findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StoresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('stores');
        $this->setDisplayField('store_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Stocktakes', [
            'foreignKey' => 'store_id'
        ]);

        // include this belongsToMany('') method as well to show the Many-to-Many relationship that it is connected to
        $this->belongsToMany('Products', [
            'through' => 'Stocktakes',
        ]);//

        //include the hasMany() method to link to the composite table
        $this->hasMany('Stocktakes', [
            'foreignKey' => 'user_store_id'
        ]); //

        // links to the Users table to get the data.
		$this->belongsToMany('Users',
							[
								'joinTable' => 'UserStores',
							]);//

        $this->hasMany('StoreSuppliers', [
            'foreignKey' => 'store_id'
        ]);
        $this->hasMany('UserStores', [
            'foreignKey' => 'store_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('store_name', 'create')
            ->notEmpty('store_name');

        $validator
            ->requirePresence('store_email', 'create')
            ->notEmpty('store_email');

        $validator
            ->requirePresence('store_phone_no', 'create')
            ->notEmpty('store_phone_no');

        $validator
            ->requirePresence('store_address', 'create')
            ->notEmpty('store_address');

        return $validator;
    }
}
