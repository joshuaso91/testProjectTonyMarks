<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Products Model
 *
 * @property \Cake\ORM\Association\HasMany $PriceChecks
 * @property \Cake\ORM\Association\HasMany $SameDayOrders
 * @property \Cake\ORM\Association\HasMany $Stocktakes
 *
 * @method \App\Model\Entity\Product get($primaryKey, $options = [])
 * @method \App\Model\Entity\Product newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Product|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Product[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Product findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('products');
        $this->setDisplayField('product_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('PriceChecks', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('SameDayOrders', [
            'foreignKey' => 'product_id'
        ]);
        // include the hasMany() method to show/connect the relationship with the composite table, in this case, Stocktakes. 
        $this->hasMany('Stocktakes', [
            'foreignKey' => 'product_id'
        ]);

        // include this belongsToMany('') method as well to show the Many-to-Many relationship that it is connected to
        $this->belongsToMany('Stores', [
            'through' => 'Stocktakes',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('product_name', 'create')
            ->notEmpty('product_name');

        $validator
            ->allowEmpty('product_unit');

        $validator
            ->allowEmpty('product_notes');

        return $validator;
    }
}
