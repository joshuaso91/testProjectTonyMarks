<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

// import the event, entity and Array Object and Time classes so that it can be used by the function. e.g. beforeMarshal
use Cake\Event\Event;
use Cake\ORM\Entity;
use ArrayObject;
use Cake\I18n\Time;

/**
 * PriceChecks Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Suppliers
 * @property \Cake\ORM\Association\BelongsTo $Products
 * @property \Cake\ORM\Association\HasMany $Orders
 *
 * @method \App\Model\Entity\PriceCheck get($primaryKey, $options = [])
 * @method \App\Model\Entity\PriceCheck newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PriceCheck[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PriceCheck|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PriceCheck patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PriceCheck[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PriceCheck findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PriceChecksTable extends Table
{
    // This function is executed before an event is save in this case. It changes the default values to a valid data so that
    // it can be saved to the database.
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options){
        $datetime = new Time();
        $data['date_entered'] = $datetime->now();
    }

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('price_checks');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Suppliers', [
            'foreignKey' => 'supplier_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'price_check_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('date_entered')
            ->allowEmpty('date_entered');

        $validator
            ->decimal('product_price')
            ->allowEmpty('product_price');

        $validator
            ->allowEmpty('notes');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['supplier_id'], 'Suppliers'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));

        return $rules;
    }
}
