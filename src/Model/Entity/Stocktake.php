<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Stocktake Entity
 *
 * @property int $id
 * @property int $store_id
 * @property int $product_id
 * @property float $current_stock_qty
 * @property float $suggested_order_qty
 * @property float $total_req_qty
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Store $store
 * @property \App\Model\Entity\Product $product
 */
class Stocktake extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
