<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Supplier Entity
 *
 * @property int $id
 * @property string $supplier_name
 * @property string $supplier_contact_name
 * @property string $supplier_email
 * @property string $supplier_contact_phone
 * @property string $supplier_phone_no
 * @property string $supplier_address
 * @property string $contact_notes
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\PriceCheck[] $price_checks
 * @property \App\Model\Entity\SameDayOrder[] $same_day_orders
 * @property \App\Model\Entity\StoreSupplier[] $store_suppliers
 */
class Supplier extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
