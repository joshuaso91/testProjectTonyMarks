<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $id
 * @property int $user_store_id
 * @property int $price_check_id
 * @property float $ordered_qty
 * @property float $ordered_date
 * @property int $created
 * @property int $modified
 *
 * @property \App\Model\Entity\UserStore $user_store
 * @property \App\Model\Entity\PriceCheck $price_check
 * @property \App\Model\Entity\Invoice[] $invoices
 */
class Order extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
