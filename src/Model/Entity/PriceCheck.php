<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PriceCheck Entity
 *
 * @property int $id
 * @property int $supplier_id
 * @property int $product_id
 * @property \Cake\I18n\FrozenTime $date_entered
 * @property float $product_price
 * @property string $notes
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Supplier $supplier
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\Order[] $orders
 */
class PriceCheck extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
