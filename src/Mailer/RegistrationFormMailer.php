<?php
namespace App\Mailer;

use Cake\Mailer\Mailer;
use Cake\Datasource\EntityInterface;

/**
 * RegistrationForm mailer.
 */
class RegistrationFormMailer extends Mailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    static public $name = 'RegistrationForm';

    public function submissionRegistration(EntityInterface $user)
    {
        $this->transport('mailjet');
        $this->from('joshuaso91@gmail.com','Joshua Song')
                ->to('joshuaso91@gmail.com')
                ->subject(sprintf('Welcome Test Templating Register Email %s', $user['user_name']))
                ->template('registration_form','registration_form')
                ->set(['data'=>$user]);
    }
}
