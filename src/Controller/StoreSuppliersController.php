<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * StoreSuppliers Controller
 *
 * @property \App\Model\Table\StoreSuppliersTable $StoreSuppliers
 *
 * @method \App\Model\Entity\StoreSupplier[] paginate($object = null, array $settings = [])
 */
class StoreSuppliersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        /* $this->paginate = [
            'contain' => ['Stores', 'Suppliers']
        ];
        $storeSuppliers = $this->paginate($this->StoreSuppliers); */

        // Use this way to find the contain Classes it belongs to if you are using DataTables instead of $this->Paginate
        $storeSuppliers = $this->StoreSuppliers->find()->contain(['Stores', 'Suppliers']);
        $this->set(compact('storeSuppliers'));
        $this->set('_serialize', ['storeSuppliers']);
    }

    /**
     * View method
     *
     * @param string|null $id Store Supplier id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $storeSupplier = $this->StoreSuppliers->get($id, [
            'contain' => ['Stores', 'Suppliers']
        ]);

        $this->set('storeSupplier', $storeSupplier);
        $this->set('_serialize', ['storeSupplier']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $storeSupplier = $this->StoreSuppliers->newEntity();
        if ($this->request->is('post')) {
            $storeSupplier = $this->StoreSuppliers->patchEntity($storeSupplier, $this->request->getData());
            if ($this->StoreSuppliers->save($storeSupplier)) {
                $this->Flash->success(__('The store supplier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The store supplier could not be saved. Please, try again.'));
        }
        $stores = $this->StoreSuppliers->Stores->find('list', ['limit' => 200]);
        $suppliers = $this->StoreSuppliers->Suppliers->find('list', ['limit' => 200]);
        $this->set(compact('storeSupplier', 'stores', 'suppliers'));
        $this->set('_serialize', ['storeSupplier']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Store Supplier id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $storeSupplier = $this->StoreSuppliers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $storeSupplier = $this->StoreSuppliers->patchEntity($storeSupplier, $this->request->getData());
            if ($this->StoreSuppliers->save($storeSupplier)) {
                $this->Flash->success(__('The store supplier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The store supplier could not be saved. Please, try again.'));
        }
        $stores = $this->StoreSuppliers->Stores->find('list', ['limit' => 200]);
        $suppliers = $this->StoreSuppliers->Suppliers->find('list', ['limit' => 200]);
        $this->set(compact('storeSupplier', 'stores', 'suppliers'));
        $this->set('_serialize', ['storeSupplier']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Store Supplier id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $storeSupplier = $this->StoreSuppliers->get($id);
        if ($this->StoreSuppliers->delete($storeSupplier)) {
            $this->Flash->success(__('The store supplier has been deleted.'));
        } else {
            $this->Flash->error(__('The store supplier could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
