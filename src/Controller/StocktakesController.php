<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Hash;
use Cake\ORM\TableRegistry;

/**
 * Stocktakes Controller
 *
 * @property \App\Model\Table\StocktakesTable $Stocktakes
 *
 * @method \App\Model\Entity\Stocktake[] paginate($object = null, array $settings = [])
 */
class StocktakesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
       /* $this->paginate = [
            'contain' => ['Stores', 'Products']
        ];
        $stocktakes = $this->paginate($this->Stocktakes); */

        // Use this way to find the contain Classes it belongs to if you are using DataTables instead of $this->Paginate
        $stocktakes = $this->Stocktakes->find()->contain(['Stores', 'Products']);
        $this->set(compact('stocktakes'));
        $this->set('_serialize', ['stocktakes']);
    }

    /**
     * View method
     *
     * @param string|null $id Stocktake id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $stocktake = $this->Stocktakes->get($id, [
            'contain' => ['Stores', 'Products']
        ]);

        $this->set('stocktake', $stocktake);
        $this->set('_serialize', ['stocktake']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
   {
       if ($this->request->is('post')) {

            $productIds = Hash::extract($this->request->getData(), 'product_id'); // extracts the product_ids array from the request
            $keys = ['store_id', 'product_id', 'current_stock_qty', 'suggested_order_qty', 'total_req_qty']; //this is just my array of keys of the column of the database

            $dataTosave = []; // this loops through the array of product _ids and generates a new value array, combines it with the keys array and pushes it to dataToSave
            foreach ( $productIds as $id ){
                $vals = [
                    Hash::get($this->request->getData(), 'store_id'),
                    $id,
                    Hash::get($this->request->getData(), 'current_stock_qty'),
                    Hash::get($this->request->getData(), 'suggested_order_qty'),
                    Hash::get($this->request->getData(), 'total_req_qty'),
                ];

                array_push($dataTosave, array_combine($keys, $vals)); // the array_combine($keys, $vals) just combines the two arrays as a key => value array
            }

            $stocktakesTable =  TableRegistry::get('Stocktakes');
               
               $stocktakes = $stocktakesTable->newEntities($dataTosave);

               $stocktakesTable->connection()->transactional(function () use ($stocktakesTable, $stocktakes) {
                foreach ($stocktakes as $stocktake) {
                    $stocktakesTable->save($stocktake, ['atomic' => false]);
                }
                $this->Flash->success(__('The stocktake has been saved.'));
                return $this->redirect(['action' => 'index']);
            });
           $this->Flash->error(__('The stocktake could not be saved. Please, try again.'));
       }
       $stocktake = $this->Stocktakes->newEntity();
       $stores = $this->Stocktakes->Stores->find('list');
       $products = $this->Stocktakes->Products->find('list');
       $this->set(compact('stocktake', 'stores', 'products'));
       $this->set('_serialize', ['stocktake']);
   }

   /**
     * Add Store products method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addStoreProducts()
   {
       if ($this->request->is('post')) {

            $productIds = Hash::extract($this->request->getData(), 'product_id'); // extracts the product_ids array from the request
            $keys = ['store_id', 'product_id', 'current_stock_qty', 'suggested_order_qty', 'total_req_qty']; //this is just my array of keys of the column of the database

            $dataTosave = []; // this loops through the array of product _ids and generates a new value array, combines it with the keys array and pushes it to dataToSave
            foreach ( $productIds as $id ){
                $vals = [
                    Hash::get($this->request->getData(), 'store_id'),
                    $id,
                    Hash::get($this->request->getData(), 'current_stock_qty'),
                    Hash::get($this->request->getData(), 'suggested_order_qty'),
                    Hash::get($this->request->getData(), 'total_req_qty'),
                ];

                array_push($dataTosave, array_combine($keys, $vals)); // the array_combine($keys, $vals) just combines the two arrays as a key => value array
            }

            $stocktakesTable =  TableRegistry::get('Stocktakes');
               
               $stocktakes = $stocktakesTable->newEntities($dataTosave);

               $stocktakesTable->connection()->transactional(function () use ($stocktakesTable, $stocktakes) {
                foreach ($stocktakes as $stocktake) {
                    $stocktakesTable->save($stocktake, ['atomic' => false]);
                }
                $this->Flash->success(__('The stocktake has been saved.'));
                return $this->redirect(['action' => 'index']);
            });
           $this->Flash->error(__('The stocktake could not be saved. Please, try again.'));
       }
       $stocktake = $this->Stocktakes->newEntity();
       $stores = $this->Stocktakes->Stores->find('list');
       $products = $this->Stocktakes->Products->find('list');
       $this->set(compact('stocktake', 'stores', 'products'));
       $this->set('_serialize', ['stocktake']);
   }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    /* public function add()
    {
        $stocktake = $this->Stocktakes->newEntity();
        if ($this->request->is('post')) {
            $stocktake = $this->Stocktakes->patchEntity($stocktake, $this->request->getData());
            if ($this->Stocktakes->save($stocktake)) {
                $this->Flash->success(__('The stocktake has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The stocktake could not be saved. Please, try again.'));
        }
        $stores = $this->Stocktakes->Stores->find('list', ['limit' => 200]);
        $products = $this->Stocktakes->Products->find('list', ['limit' => 200]);
        $this->set(compact('stocktake', 'stores', 'products'));
        $this->set('_serialize', ['stocktake']);
    } */

    /**
     * Edit method
     *
     * @param string|null $id Stocktake id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $stocktake = $this->Stocktakes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $stocktake = $this->Stocktakes->patchEntity($stocktake, $this->request->getData());
            if ($this->Stocktakes->save($stocktake)) {
                $this->Flash->success(__('The stocktake has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The stocktake could not be saved. Please, try again.'));
        }
        $stores = $this->Stocktakes->Stores->find('list');
        $products = $this->Stocktakes->Products->find('list');
        $this->set(compact('stocktake', 'stores', 'products'));
        $this->set('_serialize', ['stocktake']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Stocktake id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $stocktake = $this->Stocktakes->get($id);
        if ($this->Stocktakes->delete($stocktake)) {
            $this->Flash->success(__('The stocktake has been deleted.'));
        } else {
            $this->Flash->error(__('The stocktake could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
