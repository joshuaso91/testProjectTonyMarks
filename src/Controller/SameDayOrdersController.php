<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SameDayOrders Controller
 *
 * @property \App\Model\Table\SameDayOrdersTable $SameDayOrders
 *
 * @method \App\Model\Entity\SameDayOrder[] paginate($object = null, array $settings = [])
 */
class SameDayOrdersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        /* $this->paginate = [
            'contain' => ['UserStores', 'Suppliers', 'Products']
        ];
        $sameDayOrders = $this->paginate($this->SameDayOrders); */

        // Use this way to find the contain Classes it belongs to if you are using DataTables instead of $this->Paginate
        $sameDayOrders = $this->SameDayOrders->find()->contain(['UserStores','Suppliers', 'Products']);
        $this->set(compact('sameDayOrders'));
        $this->set('_serialize', ['sameDayOrders']);
    }

    /**
     * View method
     *
     * @param string|null $id Same Day Order id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sameDayOrder = $this->SameDayOrders->get($id, [
            'contain' => ['UserStores', 'Suppliers', 'Products', 'Invoices']
        ]);

        $this->set('sameDayOrder', $sameDayOrder);
        $this->set('_serialize', ['sameDayOrder']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sameDayOrder = $this->SameDayOrders->newEntity();
        if ($this->request->is('post')) {
            $sameDayOrder = $this->SameDayOrders->patchEntity($sameDayOrder, $this->request->getData());
            if ($this->SameDayOrders->save($sameDayOrder)) {
                $this->Flash->success(__('The same day order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The same day order could not be saved. Please, try again.'));
        }
        $userStores = $this->SameDayOrders->UserStores->find('list')->contain(
                                                                    [
                                                                        'Users' => ['fields' => ['user_name']],
                                                                        'Stores' => ['fields' => ['store_name']]
                                                                    ]);
        $suppliers = $this->SameDayOrders->Suppliers->find('list', ['limit' => 200]);
        $products = $this->SameDayOrders->Products->find('list', ['limit' => 200]);
        $this->set(compact('sameDayOrder', 'userStores', 'suppliers', 'products'));
        $this->set('_serialize', ['sameDayOrder']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Same Day Order id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sameDayOrder = $this->SameDayOrders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sameDayOrder = $this->SameDayOrders->patchEntity($sameDayOrder, $this->request->getData());
            if ($this->SameDayOrders->save($sameDayOrder)) {
                $this->Flash->success(__('The same day order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The same day order could not be saved. Please, try again.'));
        }
        $userStores = $this->SameDayOrders->UserStores->find('list')->contain(
                                                                    [
                                                                        'Users' => ['fields' => ['user_name']],
                                                                        'Stores' => ['fields' => ['store_name']]
                                                                    ]);
        $suppliers = $this->SameDayOrders->Suppliers->find('list', ['limit' => 200]);
        $products = $this->SameDayOrders->Products->find('list', ['limit' => 200]);
        $this->set(compact('sameDayOrder', 'userStores', 'suppliers', 'products'));
        $this->set('_serialize', ['sameDayOrder']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Same Day Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sameDayOrder = $this->SameDayOrders->get($id);
        if ($this->SameDayOrders->delete($sameDayOrder)) {
            $this->Flash->success(__('The same day order has been deleted.'));
        } else {
            $this->Flash->error(__('The same day order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
