<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Collection\Collection;

/**
 * PriceChecks Controller
 *
 * @property \App\Model\Table\PriceChecksTable $PriceChecks
 *
 * @method \App\Model\Entity\PriceCheck[] paginate($object = null, array $settings = [])
 */
class PriceChecksController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        /*$this->paginate = [
            'contain' => ['Suppliers', 'Products']
        ];*/
        // Use this way to find the contain Classes it belongs to if you are using DataTables instead of $this->Paginate
        $priceChecks = $this->PriceChecks->find()->contain(['Suppliers', 'Products']);
        //$priceChecks = $this->paginate($this->PriceChecks);
        $this->set(compact('priceChecks'));
        $this->set('_serialize', ['priceChecks']);
    }

    /**
     * View method
     *
     * @param string|null $id Price Check id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $priceCheck = $this->PriceChecks->get($id, [
            'contain' => ['Suppliers', 'Products', 'Orders']
        ]);

        $this->set('priceCheck', $priceCheck);
        $this->set('_serialize', ['priceCheck']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $priceCheck = $this->PriceChecks->newEntity();
        if ($this->request->is('post')) {
            $priceCheck = $this->PriceChecks->patchEntity($priceCheck, $this->request->getData());
            if ($this->PriceChecks->save($priceCheck)) {
                $this->Flash->success(__('The price check has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The price check could not be saved. Please, try again.'));
        }
        $suppliers = $this->PriceChecks->Suppliers->find('list');
        $products = $this->PriceChecks->Products->find('list');
        $this->set(compact('priceCheck', 'suppliers', 'products'));
        $this->set('_serialize', ['priceCheck']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function productList($supplier_id)
    {

        $this->viewBuilder()->layout('');

        // here we need to fetch the linked product list of supplier
        $pr_array = $this->PriceChecks->find('all')->where(['supplier_id'=>$supplier_id]);
       // we need to get the fetch data in array format so we can fetch the listed productl ist for supplier
       // list of data being found through the query above.
       // we are just extracting the 'id' of the product that's belonging to the Supplier ID into a new Collection hence the code below.
        $pr_array = (new Collection($pr_array))->extract('id')->filter()->toArray();

        //debug($pr_array->toArray());

        $products = $this->PriceChecks->Products->find('list')->where(['id IN'=>$pr_array]);
        $this->set(compact('products'));
        $this->set('_serialize', ['products']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addSupplierProducts()
    {
        $priceCheck = $this->PriceChecks->newEntity();
        if ($this->request->is('post')) {
            $priceCheck = $this->PriceChecks->patchEntity($priceCheck, $this->request->getData());
            debug($priceCheck);
            if ($this->PriceChecks->save($priceCheck)) {
                $this->Flash->success(__('The price check has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The price check could not be saved. Please, try again.'));
        }
        $suppliers = $this->PriceChecks->Suppliers->find('list');
        $products = $this->PriceChecks->Products->find('list');
        $this->set(compact('priceCheck', 'suppliers', 'products'));
        $this->set('_serialize', ['priceCheck']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Price Check id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $priceCheck = $this->PriceChecks->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $priceCheck = $this->PriceChecks->patchEntity($priceCheck, $this->request->getData());
            if ($this->PriceChecks->save($priceCheck)) {
                $this->Flash->success(__('The price check has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The price check could not be saved. Please, try again.'));
        }
        $suppliers = $this->PriceChecks->Suppliers->find('list');
        $products = $this->PriceChecks->Products->find('list');
        $this->set(compact('priceCheck', 'suppliers', 'products'));
        $this->set('_serialize', ['priceCheck']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Price Check id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $priceCheck = $this->PriceChecks->get($id);
        if ($this->PriceChecks->delete($priceCheck)) {
            $this->Flash->success(__('The price check has been deleted.'));
        } else {
            $this->Flash->error(__('The price check could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
