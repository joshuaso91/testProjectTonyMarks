<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UserStores Controller
 *
 * @property \App\Model\Table\UserStoresTable $UserStores
 *
 * @method \App\Model\Entity\UserStore[] paginate($object = null, array $settings = [])
 */
class UserStoresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        /*$this->paginate = [
            'contain' => ['Users', 'Stores']
        ];
        $userStores = $this->paginate($this->UserStores); */
        
        // Use this way to find the contain Classes it belongs to if you are using DataTables instead of $this->Paginate
        $userStores = $this->UserStores->find()->contain(['Users', 'Stores']);
        $this->set(compact('userStores'));
        $this->set('_serialize', ['userStores']);
    }

    /**
     * View method
     *
     * @param string|null $id User Store id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userStore = $this->UserStores->get($id, [
            'contain' => ['Users', 'Stores', 'Orders', 'SameDayOrders']
        ]);

        $this->set('userStore', $userStore);
        $this->set('_serialize', ['userStore']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userStore = $this->UserStores->newEntity();
        if ($this->request->is('post')) {
            $userStore = $this->UserStores->patchEntity($userStore, $this->request->getData());
            if ($this->UserStores->save($userStore)) {
                $this->Flash->success(__('The user store has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user store could not be saved. Please, try again.'));
        }
        $users = $this->UserStores->Users->find('list', ['limit' => 200]);
        $stores = $this->UserStores->Stores->find('list', ['limit' => 200]);
        $this->set(compact('userStore', 'users', 'stores'));
        $this->set('_serialize', ['userStore']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User Store id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userStore = $this->UserStores->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userStore = $this->UserStores->patchEntity($userStore, $this->request->getData());
            if ($this->UserStores->save($userStore)) {
                $this->Flash->success(__('The user store has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user store could not be saved. Please, try again.'));
        }
        $users = $this->UserStores->Users->find('list', ['limit' => 200]);
        $stores = $this->UserStores->Stores->find('list', ['limit' => 200]);
        $this->set(compact('userStore', 'users', 'stores'));
        $this->set('_serialize', ['userStore']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Store id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userStore = $this->UserStores->get($id);
        if ($this->UserStores->delete($userStore)) {
            $this->Flash->success(__('The user store has been deleted.'));
        } else {
            $this->Flash->error(__('The user store could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
