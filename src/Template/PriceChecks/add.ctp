<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
        <h4>Other Navigations:-</h4>
        <li><?= $this->Html->link(__('List Price Checks'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div id="indexAddContent" class="">
    <?= $this->Form->create($priceCheck) ?>
    <fieldset>
        <legend><?= __('Add Price Check') ?></legend>
        <?php
            echo $this->Form->control('supplier_id', ['options' => $suppliers]);
            echo $this->Form->control('product_id');
            echo $this->Form->control('date_entered', ['empty' => true]);
            echo $this->Form->control('product_price');
            echo $this->Form->control('notes');
        ?>
    </fieldset>

    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<div id="test"></div>

<script type="text/javascript">

   $(document).ready(function(){

        $('#supplier-id').on('change', function() {
            alert( this.value );
            $("#product-id").load('/testProject/price-checks/product_list/'+this.value );
        })
   
    });

</script>