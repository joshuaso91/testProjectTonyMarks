<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
        <h4>Other Navigations:-</h4>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $priceCheck->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $priceCheck->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Price Checks'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div id="indexEditContent" class="">
    <?= $this->Form->create($priceCheck) ?>
    <fieldset>
        <legend><?= __('Edit Price Check') ?></legend>
        <?php
            echo $this->Form->control('supplier_id', ['options' => $suppliers]);
            echo $this->Form->control('product_id');
            echo $this->Form->control('date_entered', ['empty' => true]);
            echo $this->Form->control('product_price');
            echo $this->Form->control('notes');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<script type="text/javascript">

   $(document).ready(function(){

        $('#supplier-id').on('change', function() {
            alert( this.value );
            $("#product-id").load('/testProject/price-checks/product_list/'+this.value );
        })
   
    });

</script>
