<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
        <h4>Other Navigations:-</h4>
        <li><?= $this->Html->link(__('New Price Check'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div id="indexContent" class="">
    <h3><?= __('Supplier Products & Price Checks') ?></h3>
    <table id="priceChecks" class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('supplier_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date_entered') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('notes') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($priceChecks as $priceCheck): ?>
            <tr>
                <td><?= $this->Number->format($priceCheck->id) ?></td>
                <td><?= $priceCheck->has('supplier') ? $this->Html->link($priceCheck->supplier->supplier_name, ['controller' => 'Suppliers', 'action' => 'view', $priceCheck->supplier->id]) : '' ?></td>
                <td><?= $priceCheck->has('product') ? $this->Html->link($priceCheck->product->product_name, ['controller' => 'Products', 'action' => 'view', $priceCheck->product->id]) : '' ?></td>
                <td><?= h($priceCheck->date_entered) ?></td>
                <td><?= $this->Number->format($priceCheck->product_price) ?></td>
                <td><?= h($priceCheck->notes) ?></td>
                <td><?= h($priceCheck->created) ?></td>
                <td><?= h($priceCheck->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $priceCheck->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $priceCheck->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $priceCheck->id], ['confirm' => __('Are you sure you want to delete # {0}?', $priceCheck->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <script>
        $(document).ready(function () {
            $('#priceChecks').DataTable();
        });
    </script>
</div>
