<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert aslert-success" onclick="this.classList.add('hidden')"><?= $message ?></div>
