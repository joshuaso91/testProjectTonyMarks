<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
        <h4>Other Navigations:-</h4>
        <li><?= $this->Html->link(__('Edit Store'), ['action' => 'edit', $store->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Store'), ['action' => 'delete', $store->id], ['confirm' => __('Are you sure you want to delete # {0}?', $store->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Stores'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Store'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Stocktakes'), ['controller' => 'Stocktakes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Stocktake'), ['controller' => 'Stocktakes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Store Suppliers'), ['controller' => 'StoreSuppliers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Store Supplier'), ['controller' => 'StoreSuppliers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Stores'), ['controller' => 'UserStores', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Store'), ['controller' => 'UserStores', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div id="indexViewContent" class="">
    <h3><?= h($store->id) ?></h3>
    <table class="table vertical-table">
        <tr>
            <th scope="row"><?= __('Store Name') ?></th>
            <td><?= h($store->store_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Store Email') ?></th>
            <td><?= h($store->store_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Store Phone No') ?></th>
            <td><?= h($store->store_phone_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Store Address') ?></th>
            <td><?= h($store->store_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($store->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($store->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($store->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Stocktakes') ?></h4>
        <?php if (!empty($store->stocktakes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Store Id') ?></th>
                <th scope="col"><?= __('Product Id') ?></th>
                <th scope="col"><?= __('Current Stock Qty') ?></th>
                <th scope="col"><?= __('Suggested Order Qty') ?></th>
                <th scope="col"><?= __('Total Req Qty') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($store->stocktakes as $stocktakes): ?>
            <tr>
                <td><?= h($stocktakes->id) ?></td>
                <td><?= h($stocktakes->store_id) ?></td>
                <td><?= h($stocktakes->product_id) ?></td>
                <td><?= h($stocktakes->current_stock_qty) ?></td>
                <td><?= h($stocktakes->suggested_order_qty) ?></td>
                <td><?= h($stocktakes->total_req_qty) ?></td>
                <td><?= h($stocktakes->created) ?></td>
                <td><?= h($stocktakes->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Stocktakes', 'action' => 'view', $stocktakes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Stocktakes', 'action' => 'edit', $stocktakes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Stocktakes', 'action' => 'delete', $stocktakes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stocktakes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Store Suppliers') ?></h4>
        <?php if (!empty($store->store_suppliers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Store Id') ?></th>
                <th scope="col"><?= __('Supplier Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($store->store_suppliers as $storeSuppliers): ?>
            <tr>
                <td><?= h($storeSuppliers->id) ?></td>
                <td><?= h($storeSuppliers->store_id) ?></td>
                <td><?= h($storeSuppliers->supplier_id) ?></td>
                <td><?= h($storeSuppliers->created) ?></td>
                <td><?= h($storeSuppliers->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'StoreSuppliers', 'action' => 'view', $storeSuppliers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'StoreSuppliers', 'action' => 'edit', $storeSuppliers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'StoreSuppliers', 'action' => 'delete', $storeSuppliers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $storeSuppliers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related User Stores') ?></h4>
        <?php if (!empty($store->user_stores)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Store Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($store->user_stores as $userStores): ?>
            <tr>
                <td><?= h($userStores->id) ?></td>
                <td><?= h($userStores->user_id) ?></td>
                <td><?= h($userStores->store_id) ?></td>
                <td><?= h($userStores->created) ?></td>
                <td><?= h($userStores->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserStores', 'action' => 'view', $userStores->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserStores', 'action' => 'edit', $userStores->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserStores', 'action' => 'delete', $userStores->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userStores->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
