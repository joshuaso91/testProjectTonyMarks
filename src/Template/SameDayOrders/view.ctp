<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
        <h4>Other Navigations:-</h4>
        <li><?= $this->Html->link(__('Edit Same Day Order'), ['action' => 'edit', $sameDayOrder->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Same Day Order'), ['action' => 'delete', $sameDayOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sameDayOrder->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Same Day Orders'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Same Day Order'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Stores'), ['controller' => 'UserStores', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Store'), ['controller' => 'UserStores', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Invoices'), ['controller' => 'Invoices', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Invoice'), ['controller' => 'Invoices', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div id="indexViewContent" class="">
    <h3><?= h($sameDayOrder->id) ?></h3>
    <table class="table vertical-table">
        <tr>
            <th scope="row"><?= __('User Store') ?></th>
            <td><?= $sameDayOrder->has('user_store') ? $this->Html->link($sameDayOrder->user_store->id, ['controller' => 'UserStores', 'action' => 'view', $sameDayOrder->user_store->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Supplier') ?></th>
            <td><?= $sameDayOrder->has('supplier') ? $this->Html->link($sameDayOrder->supplier->id, ['controller' => 'Suppliers', 'action' => 'view', $sameDayOrder->supplier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Product') ?></th>
            <td><?= $sameDayOrder->has('product') ? $this->Html->link($sameDayOrder->product->id, ['controller' => 'Products', 'action' => 'view', $sameDayOrder->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Same Day Order') ?></th>
            <td><?= h($sameDayOrder->same_day_order) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($sameDayOrder->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ordered Qty') ?></th>
            <td><?= $this->Number->format($sameDayOrder->ordered_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Date') ?></th>
            <td><?= h($sameDayOrder->order_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($sameDayOrder->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($sameDayOrder->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Order Notes') ?></h4>
        <?= $this->Text->autoParagraph(h($sameDayOrder->order_notes)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Invoices') ?></h4>
        <?php if (!empty($sameDayOrder->invoices)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Order Id') ?></th>
                <th scope="col"><?= __('Same Day Order Id') ?></th>
                <th scope="col"><?= __('Invoice Date') ?></th>
                <th scope="col"><?= __('Invoice Notes') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($sameDayOrder->invoices as $invoices): ?>
            <tr>
                <td><?= h($invoices->id) ?></td>
                <td><?= h($invoices->order_id) ?></td>
                <td><?= h($invoices->same_day_order_id) ?></td>
                <td><?= h($invoices->invoice_date) ?></td>
                <td><?= h($invoices->invoice_notes) ?></td>
                <td><?= h($invoices->created) ?></td>
                <td><?= h($invoices->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Invoices', 'action' => 'view', $invoices->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Invoices', 'action' => 'edit', $invoices->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Invoices', 'action' => 'delete', $invoices->id], ['confirm' => __('Are you sure you want to delete # {0}?', $invoices->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
