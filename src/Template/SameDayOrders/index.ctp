<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
        <h4>Other Navigations:-</h4>
        <li><?= $this->Html->link(__('New Same Day Order'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Stores'), ['controller' => 'UserStores', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Store'), ['controller' => 'UserStores', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Invoices'), ['controller' => 'Invoices', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Invoice'), ['controller' => 'Invoices', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div id="indexContent" class="">
    <h3><?= __('Same Day Orders') ?></h3>
    <table id="sameDayOrders" class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_store_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('supplier_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ordered_qty') ?></th>
                <th scope="col"><?= $this->Paginator->sort('order_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('same_day_order') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($sameDayOrders as $sameDayOrder): ?>
            <tr>
                <td><?= $this->Number->format($sameDayOrder->id) ?></td>
                <td><?= $sameDayOrder->has('user_store') ? $this->Html->link($sameDayOrder->user_store->id, ['controller' => 'UserStores', 'action' => 'view', $sameDayOrder->user_store->id]) : '' ?></td>
                <td><?= $sameDayOrder->has('supplier') ? $this->Html->link($sameDayOrder->supplier->supplier_name, ['controller' => 'Suppliers', 'action' => 'view', $sameDayOrder->supplier->id]) : '' ?></td>
                <td><?= $sameDayOrder->has('product') ? $this->Html->link($sameDayOrder->product->product_name, ['controller' => 'Products', 'action' => 'view', $sameDayOrder->product->id]) : '' ?></td>
                <td><?= $this->Number->format($sameDayOrder->ordered_qty) ?></td>
                <td><?= h($sameDayOrder->order_date) ?></td>
                <td><?= h($sameDayOrder->same_day_order) ?></td>
                <td><?= h($sameDayOrder->created) ?></td>
                <td><?= h($sameDayOrder->modified) ?></td>
                <td class="actions">
                    <?php echo $this->Html->link('<span class = "glyphicon glyphicon-eye-open"></span>',['action' => 'view', $sameDayOrder->id],['class' => 'btn btn-primary', 'role' => 'button', 'escape' => false]);?>
					<?php echo $this->Html->link('<span class = "glyphicon glyphicon-pencil"></span>',['action' => 'edit', $sameDayOrder->id],['class' => 'btn btn-success', 'role' => 'button', 'escape' => false]);?>
					<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sameDayOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sameDayOrder->id), 'class' => 'btn btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
    <script>
        $(document).ready(function () {
            $('#sameDayOrders').DataTable();
        });
    </script>
</div>
