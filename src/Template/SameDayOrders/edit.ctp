<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
        <h4>Other Navigations:-</h4>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $sameDayOrder->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $sameDayOrder->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Same Day Orders'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List User Stores'), ['controller' => 'UserStores', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Store'), ['controller' => 'UserStores', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Invoices'), ['controller' => 'Invoices', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Invoice'), ['controller' => 'Invoices', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div id="indexEditContent" class="">
    <?= $this->Form->create($sameDayOrder) ?>
    <fieldset>
        <legend><?= __('Edit Same Day Order') ?></legend>
        <?php
            echo $this->Form->control('user_store_id', ['options' => $userStores]);
            echo $this->Form->control('supplier_id', ['options' => $suppliers]);
            echo $this->Form->control('product_id', ['options' => $products]);
            echo $this->Form->control('ordered_qty');
            echo $this->Form->control('order_date');
            echo $this->Form->control('same_day_order');
            echo $this->Form->control('order_notes');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
