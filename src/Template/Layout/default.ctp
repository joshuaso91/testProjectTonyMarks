<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <!-- CSS - Cascading Style Sheet -->
    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('tonymarks_custom.css') ?>
    
    <!-- inserting jquery into the entire page -->
    <!-- Javascript -->
    <?= $this->Html->script('jquery.js') ?>
    <?= $this->Html->script('bootstrap.js') ?>
    <?= $this->Html->script('custom.js') ?>
    

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>

</head>
<body>
<?php if($superAdmin || $admin || $staff): ?>
<nav class="navbar backgroundNav">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#topFixedNavbar1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<?php
				$logo_img = $this->Html->image('logo_minified_2.png', array('class' => ''));
				echo $this->Html->link( $logo_img, array('controller'=>'UserStores','action'=>'index'), array('escape'=>false));
			?>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-right" id="topFixedNavbar1">
			<ul class="nav navbar-nav">
				<?php if($loggedIn): ?>
					<!-- *********************************************************** -->
					<!-- I've set and added the User loggedIn name and the Users role at the top of the navigation bar. 
						 I did it through the app controller -->
					<li id="loggedInWelcome" class="">Welcome, <?= $users_username; ?>:&nbsp;<?= $users_roles; ?></li>
					<li><?= $this->Html->link('Logout', ['controller'=>'users', 'action'=>'logout'], array('class' => 'trans02s logoutButton')); ?></li>
				<?php else: ?>
					<li><?= $this->Html->link('Register', ['controller'=>'users', 'action'=>'register'], array('class' => 'navFont trans02s')); ?></li>
					<li><?= $this->Html->link('Login', ['controller'=>'users', 'action'=>'login'], array('class' => 'navFont trans02s')); ?></li>
				<?php endif; ?>
			</ul>
		</div>
      <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<!-- Side bar navigation -->
<div id="sideBar_Nav" class="">
	<div class="row">
		<div class="wrapper">
    	    <div class="side-bar">
                <ul>
                    <li class="menu-head">
    					<?php /*
							$span_glyphicon = $this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-th-large pull-right'));
							echo $this->Html->link("Dashboard $span_glyphicon",array('controller' => 'UserStores', 'action' => '#'),
							array('class' => 'push_menu navFont', 'escape' => false)) */
						?>
                   		<a href="#" class="push_menu navFont">Dashboard <span class="glyphicon glyphicon-th-large pull-right"</span></a>
                    </li>
                    <div class="menu trans02s">
						<li data-toggle="collapse" data-target="#usersSubMenu">
                        	<a href="#" class="">User Accounts <span class="glyphicon glyphicon-tags pull-right"></span></a>
                        	<ul id="usersSubMenu" class="collapse">
                        		<li>
                        			<a href="#" class="sideBarFont">New Users<span class="glyphicon glyphicon-plus pull-right"</span></a>
                        		</li>
                        		<li>
                        			<a href="#" class="sideBarFont">List Users<span class="glyphicon glyphicon-plus pull-right"</span></a>
                        		</li>
                        	</ul>
                        </li>
                        <li data-toggle="collapse" data-target="#supplierSubMenu">
                        	<a href="#" class="">Suppliers <span class="glyphicon glyphicon-tags pull-right"></span></a>
                        	<ul id="supplierSubMenu" class="collapse">
                        		<li>
                        			<a href="#" class="sideBarFont">Price Check <span class="glyphicon glyphicon-plus pull-right"</span></a>
                        		</li>
                        		<li>
                        			<a href="#" class="sideBarFont">Add Products<span class="glyphicon glyphicon-plus pull-right"</span></a>
                        		</li>
                        		<li>
                        			<a href="#" class="sideBarFont">List Products<span class="glyphicon glyphicon-pencil pull-right"</span></a>
                        		</li>
                        	</ul>
                        </li>
                        <li data-toggle="collapse" data-target="#orderSubMenu">
                        	<a href="#" class="">Orders<span class="glyphicon glyphicon-shopping-cart pull-right"></span></a>
                        	<ul id="orderSubMenu" class="collapse">
                        		<li>
                        			<a href="#" class="sideBarFont">List Orders</a>
                        		</li>
                        		<li>
                        			<a href="#" class="sideBarFont">List Same-Day Orders</span></a>
                        		</li>
                        		<li>
                        			<a href="#" class="sideBarFont">Add Orders</span></a>
                        		</li>
								<li>
                        			<a href="#" class="sideBarFont">Add Same-day Orders</span></a>
                        		</li>
                        	</ul>
                        </li>
                        <li>
                            <a href="#" class="">Buying<span class="glyphicon glyphicon-usd pull-right"></span></a>
                        </li>
                        <li>
                            <a href="#" class="">Admin<span class="glyphicon glyphicon-cog pull-right"></span></a>
                        </li>
                    </div>
                    
                </ul>
    	    </div>   
            <div class="content">
                <div class="container-fluid col-md-12">
                    <div class="container-fluid clearfix padding-left-right-0">
						<?= $this->Flash->render() ?>

						<!-- Preloader loading circle [implement it next time]

							<div id="loader-wrapper">
							<div id="loaders">

							</div>
						</div> -->

						<?= $this->fetch('content') ?>
					</div>
                </div>
            </div>
		</div>
	</div>
</div>
<!-- End of Side bar navigation -->
<?php else: ?>
	<div class="container-fluid clearfix padding-left-right-0">
		<?= $this->Flash->render() ?>
		<?= $this->fetch('content') ?>
	</div>
<?php endif; ?>

    
    
    
    <footer>
    </footer>
</body>
</html>

<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>