<!-- Top content -->
<div id="loginPage-container" class="container-fluid">
	<div id="loginContainer_outer" class="container container-fluid">
	
		<div id="loginContainer_inner" class="container-fluid">
			<div id="loginContainer_header" class="">
			<?= $this->Html->image('logo.png', array('class' => '')) ?>
			</div>
			
			<div id="loginContainer_body" class="container-fluid">
			<?php echo $this->Flash->render('message') ?>
				<?= $this->Form->create(); ?>
					<?= $this->Form->input('user_email'); ?>
					<?= $this->Form->input('password', array('type' => 'password')); ?>
					<?= $this->Form->submit('Login', array('class'=>'btn btn-primary trans02s')); ?>
				<?= $this->Form->end(); ?>
				
				<div id="">
					<h4>OR</h4>
					<?= $this->Html->link('Register', ['controller'=>'users', 'action'=>'register'], array('class' => 'btn btnLogin trans02s')); ?>
				</div>
				
			</div>
			
		</div>
		
	</div>
</div>