<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
        <h4>Other Navigations:-</h4>
        <li><?= $this->Html->link(__('New Store Supplier'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Stores'), ['controller' => 'Stores', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Store'), ['controller' => 'Stores', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div id="indexContent" class="">
    <h3><?= __('Store Suppliers') ?></h3>
    <table id="storeSuppliers" class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('store_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('supplier_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($storeSuppliers as $storeSupplier): ?>
            <tr>
                <td><?= $this->Number->format($storeSupplier->id) ?></td>
                <td><?= $storeSupplier->has('store') ? $this->Html->link($storeSupplier->store->store_name, ['controller' => 'Stores', 'action' => 'view', $storeSupplier->store->id]) : '' ?></td>
                <td><?= $storeSupplier->has('supplier') ? $this->Html->link($storeSupplier->supplier->supplier_name, ['controller' => 'Suppliers', 'action' => 'view', $storeSupplier->supplier->id]) : '' ?></td>
                <td><?= h($storeSupplier->created) ?></td>
                <td><?= h($storeSupplier->modified) ?></td>
                <td class="actions">
                    <?php echo $this->Html->link('<span class = "glyphicon glyphicon-eye-open"></span>',['action' => 'view', $storeSupplier->id],['class' => 'btn btn-primary', 'role' => 'button', 'escape' => false]);?>
					<?php echo $this->Html->link('<span class = "glyphicon glyphicon-pencil"></span>',['action' => 'edit', $storeSupplier->id],['class' => 'btn btn-success', 'role' => 'button', 'escape' => false]);?>
					<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $storeSupplier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $storeSupplier->id), 'class' => 'btn btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <script>
        $(document).ready(function () {
            $('#storeSuppliers').DataTable();
        });
    </script>
</div>
