<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
        <h4>Other Navigations:-</h4>
        <li><?= $this->Html->link(__('Edit Store Supplier'), ['action' => 'edit', $storeSupplier->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Store Supplier'), ['action' => 'delete', $storeSupplier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $storeSupplier->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Store Suppliers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Store Supplier'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Stores'), ['controller' => 'Stores', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Store'), ['controller' => 'Stores', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div id="indexViewContent" class="">
    <h3><?= h($storeSupplier->id) ?></h3>
    <table class="table vertical-table">
        <tr>
            <th scope="row"><?= __('Store') ?></th>
            <td><?= $storeSupplier->has('store') ? $this->Html->link($storeSupplier->store->id, ['controller' => 'Stores', 'action' => 'view', $storeSupplier->store->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Supplier') ?></th>
            <td><?= $storeSupplier->has('supplier') ? $this->Html->link($storeSupplier->supplier->id, ['controller' => 'Suppliers', 'action' => 'view', $storeSupplier->supplier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($storeSupplier->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($storeSupplier->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($storeSupplier->modified) ?></td>
        </tr>
    </table>
</div>
