<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
        <h4>Other Navigations:-</h4>
        <li><?= $this->Html->link(__('New Stocktake'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Stores'), ['controller' => 'Stores', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Store'), ['controller' => 'Stores', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div id="indexContent" class="">
    <h3><?= __('Store Products & Stocktakes') ?></h3>
    <table id="stocktakes" class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('store_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('current_stock_qty') ?></th>
                <th scope="col"><?= $this->Paginator->sort('suggested_order_qty') ?></th>
                <th scope="col"><?= $this->Paginator->sort('total_req_qty') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($stocktakes as $stocktake): ?>
            <tr>
                <td><?= $this->Number->format($stocktake->id) ?></td>
                <td><?= $stocktake->has('store') ? $this->Html->link($stocktake->store->store_name, ['controller' => 'Stores', 'action' => 'view', $stocktake->store->id]) : '' ?></td>
                <td><?= $stocktake->has('product') ? $this->Html->link($stocktake->product->product_name, ['controller' => 'Products', 'action' => 'view', $stocktake->product->id]) : '' ?></td>
                <td><?= $this->Number->format($stocktake->current_stock_qty) ?></td>
                <td><?= $this->Number->format($stocktake->suggested_order_qty) ?></td>
                <td><?= $this->Number->format($stocktake->total_req_qty) ?></td>
                <td><?= h($stocktake->created) ?></td>
                <td><?= h($stocktake->modified) ?></td>
                <td class="actions">
                    <?php echo $this->Html->link('<span class = "glyphicon glyphicon-eye-open"></span>',['action' => 'view', $stocktake->id],['class' => 'btn btn-primary', 'role' => 'button', 'escape' => false]);?>
					<?php echo $this->Html->link('<span class = "glyphicon glyphicon-pencil"></span>',['action' => 'edit', $stocktake->id],['class' => 'btn btn-success', 'role' => 'button', 'escape' => false]);?>
					<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $stocktake->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stocktake->id), 'class' => 'btn btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <script>
        $(document).ready(function () {
            $('#stocktakes').DataTable();
        });
    </script>
</div>
