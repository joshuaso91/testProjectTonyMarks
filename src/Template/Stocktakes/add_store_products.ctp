<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
        <h4>Other Navigations:-</h4>
        <li><?= $this->Html->link(__('List Stocktakes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Stores'), ['controller' => 'Stores', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Store'), ['controller' => 'Stores', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div id="indexAddContent" class="">
    <?= $this->Form->create($stocktake) ?>
    <fieldset>
        <legend><?= __('Add Stocktake') ?></legend>
        <?php
            echo $this->Form->control('store_id', ['options' => $stores]);
            echo $this->Form->control('product_id', ['type'=>'select','multiple'=>true,
                                                    'options' => $products
                                                    ]);
            echo $this->Form->hidden('current_stock_qty');
            echo $this->Form->hidden('suggested_order_qty');
            echo $this->Form->hidden('total_req_qty');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
