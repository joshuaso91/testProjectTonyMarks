<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
        <h4>Other Navigations:-</h4>
        <li><?= $this->Html->link(__('Edit Stocktake'), ['action' => 'edit', $stocktake->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Stocktake'), ['action' => 'delete', $stocktake->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stocktake->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Stocktakes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Stocktake'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Stores'), ['controller' => 'Stores', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Store'), ['controller' => 'Stores', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div id="indexViewContent" class="">
    <h3><?= h($stocktake->id) ?></h3>
    <table class="table vertical-table">
        <tr>
            <th scope="row"><?= __('Store') ?></th>
            <td><?= $stocktake->has('store') ? $this->Html->link($stocktake->store->id, ['controller' => 'Stores', 'action' => 'view', $stocktake->store->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Product') ?></th>
            <td><?= $stocktake->has('product') ? $this->Html->link($stocktake->product->id, ['controller' => 'Products', 'action' => 'view', $stocktake->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($stocktake->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Current Stock Qty') ?></th>
            <td><?= $this->Number->format($stocktake->current_stock_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Suggested Order Qty') ?></th>
            <td><?= $this->Number->format($stocktake->suggested_order_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total Req Qty') ?></th>
            <td><?= $this->Number->format($stocktake->total_req_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($stocktake->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($stocktake->modified) ?></td>
        </tr>
    </table>
</div>
