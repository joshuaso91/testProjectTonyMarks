<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
        <h4>Other Navigations:-</h4>
        <li><?= $this->Html->link(__('New Invoice'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Same Day Orders'), ['controller' => 'SameDayOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Same Day Order'), ['controller' => 'SameDayOrders', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div id="indexContent" class="">
    <h3><?= __('Invoices') ?></h3>
    <table id="invoices" class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('order_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('same_day_order_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('invoice_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($invoices as $invoice): ?>
            <tr>
                <td><?= $this->Number->format($invoice->id) ?></td>
                <td><?= $invoice->has('order') ? $this->Html->link($invoice->order->id, ['controller' => 'Orders', 'action' => 'view', $invoice->order->id]) : '' ?></td>
                <td><?= $invoice->has('same_day_order') ? $this->Html->link($invoice->same_day_order->id, ['controller' => 'SameDayOrders', 'action' => 'view', $invoice->same_day_order->id]) : '' ?></td>
                <td><?= h($invoice->invoice_date) ?></td>
                <td><?= h($invoice->created) ?></td>
                <td><?= h($invoice->modified) ?></td>
                <td class="actions">
                    <?php echo $this->Html->link('<span class = "glyphicon glyphicon-eye-open"></span>',['action' => 'view', $invoice->id],['class' => 'btn btn-primary', 'role' => 'button', 'escape' => false]);?>
					<?php echo $this->Html->link('<span class = "glyphicon glyphicon-pencil"></span>',['action' => 'edit', $invoice->id],['class' => 'btn btn-success', 'role' => 'button', 'escape' => false]);?>
					<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $invoice->id], ['confirm' => __('Are you sure you want to delete # {0}?', $invoice->id), 'class' => 'btn btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <script>
        $(document).ready(function () {
            $('#invoices').DataTable();
        });
    </script>
</div>
