<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
        <h4>Other Navigations:-</h4>
        <li><?= $this->Html->link(__('List Suppliers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Price Checks'), ['controller' => 'PriceChecks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Price Check'), ['controller' => 'PriceChecks', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Same Day Orders'), ['controller' => 'SameDayOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Same Day Order'), ['controller' => 'SameDayOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Store Suppliers'), ['controller' => 'StoreSuppliers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Store Supplier'), ['controller' => 'StoreSuppliers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div id="indexAddContent" class="">
    <?= $this->Form->create($supplier) ?>
    <fieldset>
        <legend><?= __('Add Supplier') ?></legend>
        <?php
            echo $this->Form->control('supplier_name');
            echo $this->Form->control('supplier_contact_name');
            echo $this->Form->control('supplier_email');
            echo $this->Form->control('supplier_contact_phone');
            echo $this->Form->control('supplier_phone_no');
            echo $this->Form->control('supplier_address');
            echo $this->Form->control('contact_notes');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
