<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Supplier'), ['action' => 'edit', $supplier->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Supplier'), ['action' => 'delete', $supplier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $supplier->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Suppliers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Supplier'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Price Checks'), ['controller' => 'PriceChecks', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Price Check'), ['controller' => 'PriceChecks', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Same Day Orders'), ['controller' => 'SameDayOrders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Same Day Order'), ['controller' => 'SameDayOrders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Store Suppliers'), ['controller' => 'StoreSuppliers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Store Supplier'), ['controller' => 'StoreSuppliers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="suppliers view large-9 medium-8 columns content">
    <h3><?= h($supplier->supplier_name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Supplier Name') ?></th>
            <td><?= h($supplier->supplier_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Supplier Contact Name') ?></th>
            <td><?= h($supplier->supplier_contact_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Supplier Email') ?></th>
            <td><?= h($supplier->supplier_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Supplier Contact Phone') ?></th>
            <td><?= h($supplier->supplier_contact_phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Supplier Phone No') ?></th>
            <td><?= h($supplier->supplier_phone_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Supplier Address') ?></th>
            <td><?= h($supplier->supplier_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contact Notes') ?></th>
            <td><?= h($supplier->contact_notes) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($supplier->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($supplier->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($supplier->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Price Checks') ?></h4>
        <?php if (!empty($supplier->price_checks)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Supplier Id') ?></th>
                <th scope="col"><?= __('Product Id') ?></th>
                <th scope="col"><?= __('Date Entered') ?></th>
                <th scope="col"><?= __('Product Price') ?></th>
                <th scope="col"><?= __('Notes') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($supplier->price_checks as $priceChecks): ?>
            <tr>
                <td><?= h($priceChecks->id) ?></td>
                <td><?= h($priceChecks->supplier_id) ?></td>
                <td><?= h($priceChecks->product_id) ?></td>
                <td><?= h($priceChecks->date_entered) ?></td>
                <td><?= h($priceChecks->product_price) ?></td>
                <td><?= h($priceChecks->notes) ?></td>
                <td><?= h($priceChecks->created) ?></td>
                <td><?= h($priceChecks->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PriceChecks', 'action' => 'view', $priceChecks->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PriceChecks', 'action' => 'edit', $priceChecks->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PriceChecks', 'action' => 'delete', $priceChecks->id], ['confirm' => __('Are you sure you want to delete # {0}?', $priceChecks->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Same Day Orders') ?></h4>
        <?php if (!empty($supplier->same_day_orders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Store Id') ?></th>
                <th scope="col"><?= __('Supplier Id') ?></th>
                <th scope="col"><?= __('Product Id') ?></th>
                <th scope="col"><?= __('Ordered Qty') ?></th>
                <th scope="col"><?= __('Order Date') ?></th>
                <th scope="col"><?= __('Same Day Order') ?></th>
                <th scope="col"><?= __('Order Notes') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($supplier->same_day_orders as $sameDayOrders): ?>
            <tr>
                <td><?= h($sameDayOrders->id) ?></td>
                <td><?= h($sameDayOrders->user_store_id) ?></td>
                <td><?= h($sameDayOrders->supplier_id) ?></td>
                <td><?= h($sameDayOrders->product_id) ?></td>
                <td><?= h($sameDayOrders->ordered_qty) ?></td>
                <td><?= h($sameDayOrders->order_date) ?></td>
                <td><?= h($sameDayOrders->same_day_order) ?></td>
                <td><?= h($sameDayOrders->order_notes) ?></td>
                <td><?= h($sameDayOrders->created) ?></td>
                <td><?= h($sameDayOrders->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SameDayOrders', 'action' => 'view', $sameDayOrders->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'SameDayOrders', 'action' => 'edit', $sameDayOrders->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SameDayOrders', 'action' => 'delete', $sameDayOrders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sameDayOrders->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Store Suppliers') ?></h4>
        <?php if (!empty($supplier->store_suppliers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Store Id') ?></th>
                <th scope="col"><?= __('Supplier Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($supplier->store_suppliers as $storeSuppliers): ?>
            <tr>
                <td><?= h($storeSuppliers->id) ?></td>
                <td><?= h($storeSuppliers->store_id) ?></td>
                <td><?= h($storeSuppliers->supplier_id) ?></td>
                <td><?= h($storeSuppliers->created) ?></td>
                <td><?= h($storeSuppliers->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'StoreSuppliers', 'action' => 'view', $storeSuppliers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'StoreSuppliers', 'action' => 'edit', $storeSuppliers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'StoreSuppliers', 'action' => 'delete', $storeSuppliers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $storeSuppliers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
