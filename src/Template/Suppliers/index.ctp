<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
        <h4>Other Navigations:-</h4>
        <li><?= $this->Html->link(__('New Supplier'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Price Checks'), ['controller' => 'PriceChecks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Price Check'), ['controller' => 'PriceChecks', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Same Day Orders'), ['controller' => 'SameDayOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Same Day Order'), ['controller' => 'SameDayOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Store Suppliers'), ['controller' => 'StoreSuppliers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Store Supplier'), ['controller' => 'StoreSuppliers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div id="indexContent" class="">
    <h3><?= __('Suppliers') ?></h3>
    <table id="suppliers" class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('supplier_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('supplier_contact_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('supplier_email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('supplier_contact_phone') ?></th>
                <th scope="col"><?= $this->Paginator->sort('supplier_phone_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('supplier_address') ?></th>
                <th scope="col"><?= $this->Paginator->sort('contact_notes') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($suppliers as $supplier): ?>
            <tr>
                <td><?= $this->Number->format($supplier->id) ?></td>
                <td><?= h($supplier->supplier_name) ?></td>
                <td><?= h($supplier->supplier_contact_name) ?></td>
                <td><?= h($supplier->supplier_email) ?></td>
                <td><?= h($supplier->supplier_contact_phone) ?></td>
                <td><?= h($supplier->supplier_phone_no) ?></td>
                <td><?= h($supplier->supplier_address) ?></td>
                <td><?= h($supplier->contact_notes) ?></td>
                <td><?= h($supplier->created) ?></td>
                <td><?= h($supplier->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $supplier->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $supplier->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $supplier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $supplier->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <script>
        $(document).ready(function () {
            $('#suppliers').DataTable();
        });
    </script>
</div>
